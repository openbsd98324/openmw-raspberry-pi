# openmw-raspberry-pi

## Introduction

opensource openmw, morrowind, running on raspberry pi (tested on rpi3b+, plus). 

First attempt. It runs very fast and smoothly. The PI is rpi3b plus.

![](media/1637354970-screenshot.png)


Using VC video driver:

![](media/1638706039-screenshot.png)




## External harddisk 

The rootfs is on the sda2 harddisk (usb external, WD extension).

The SD/MMC has the kernel, to boot on sda2.


## Rootfs / image

The files are available on this website : 

https://gitlab.com/openbsd98324/openmw-raspberry-pi/-/tree/main/releases/v1

## Installation and configuration

apt-get install openmw


Use the wizard, from an Installation (location), and use the esm files.

````
Adding BSA archive C:\GOG Games\Morrowind\Data Files\Morrowind.esm
Adding BSA archive C:\GOG Games\Morrowind\Data Files\Tribunal.esm
Adding BSA archive C:\GOG Games\Morrowind\Data Files\Bloodmoon.esm
````




